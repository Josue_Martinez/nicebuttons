/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    
    // DEFAULT VALUES
    
    var states = 'default';
    var frame = 'default';
    
    var button_css = new Object();
    var button_hover_css = new Object();
    
    button_css['width'] = '205px';
    button_css['height'] = '128px';
    button_css['line-height'] = '128px';
    button_css['border-width'] = '0px';
    button_css['border-color'] = '#000000';
    button_css['border-style'] = 'solid';
    button_css['border-radius'] = '0px';
    button_css['background-color'] = '#C8C8C8';
    button_css['color'] = '#333333';
    button_css['font-size'] = '50px';
    button_css['opacity'] = '1';
    
    button_css['-moz-box-shadow'] = 'none';
    button_css['-webkit-box-shadow'] = 'none';
    button_css['box-shadow'] = 'none';
    
    button_css['transition-property'] = 'all';
    button_css['transition-duration'] = '0s';
    button_css['transition-timing-function'] = 'ease';
    button_css['transition-delay'] = '0s';
    
    button_css['transform'] = 'none';
    button_css['-ms-transform'] = 'none';
    button_css['-webkit-transform'] = 'none';
    
    button_css['transform-style'] = 'flat';
    button_css['-webkit-transform-style'] = 'flat';
    
    button_css['perspective'] = 'none';
    button_css['perspective-origin'] = '50% 50%';
    
    
    var translate_x = 0;
    var translate_y = 0;
    
    var scale_x = 0;
    var scale_y = 0;
    
    var origin_x = '50%';
    var origin_y = '50%';
    var origin_z = 0;
    
    var perspective_origin_x = '50%';
    var perspective_origin_y = '50%';
 
    
    
    $('#button').draggable({ containment: "#result" });
    //$('#button').resizable();
    
    var fontsize = $('#button').css('font-size');
    fontsize = Number(fontsize.slice(0,-2));
    $('#button-fontsize').val(fontsize);
    
    $('#button-text').val($('#button').html());
    
    var fontcolor = $('#button').css('color');
    $('#button-color').val(fontcolor);
    
    var backgroundcolor = $('#button').css('background-color');
    $('#button-bgcolor').val(backgroundcolor);
    
    var width = $('#button').css('width');
    width = Number(width.slice(0,-2));
    $('#button-width').val(width);
    
    var height = $('#button').css('height');
    height = Number(height.slice(0,-2));
    $('#button-height').val(height);
    
    var borderwidth = $('#button').css('border-width');
    borderwidth = Number(borderwidth.slice(0,-2));
    $('#button-border').val(borderwidth);
    
    var bordercolor = $('#button').css('border-color');
    $('#button-bordercolor').val(bordercolor);
    
    var bordertype = $('#button').css('border-style');
    $('#button-bordertype').find('option[value="'+bordertype+'"]').attr("selected", "selected");
    
    var radius = $('#button').css('border-radius');
    radius = Number(radius.slice(0,-2));
    $('#button-borderradius').val(radius);
    
    // TERMINAR DE INICIALIZAR LOS INPUT A LOS VALORES DEL BUTTON
    
    /*
     * TOOLBOX INPUT FUNCTIONS
     */
    
    $('#button-text').keyup(function(){
       
       $('#button').html($(this).val());
        
    });
    
    $('#button-fontsize').keyup(function(){
       
       load_css('font-size', $(this).val()+'px');
        
    });
    $('#button-fontsize').change(function(){
       
       load_css('font-size', $(this).val()+'px');
        
    });
    
    $('#button-color').change(function(){
       
       load_css('color', $(this).val());
        
    });
    
    $('#button-bgcolor').change(function(){
       
       load_css('background-color', $(this).val());
        
    });
    
    $('#button-width').change(function(){
       
       load_css('width', $(this).val());
        
    });
    
    $('#button-height').change(function(){
        
        
        load_css('height', $(this).val());
        load_css('line-height', $(this).val()+'px');
        
    });
    
    $('#button-border').change(function(){
        
        load_css('border-width', $(this).val()+'px');
        
    });
    $('#button-bordercolor').change(function(){
        
        load_css('border-color', $(this).val());
        
    });
    $('#button-bordertype').change(function(){
        
        load_css('border-style', $(this).val());
        
    });
    $('#button-borderradius').change(function(){
        
        load_css('border-radius', $(this).val()+'px');
        
    });
    $('#button-opacity').change(function(){
        
        load_css('opacity', $(this).val());
        
    });
    $('#button-boxshadow').change(function(){
        
        load_css('-webkit-box-shadow', $(this).val()+'px '+$(this).val()+'px '+$(this).val()+'px #333');
        load_css('-moz-box-shadow', $(this).val()+'px '+$(this).val()+'px '+$(this).val()+'px #333');
        load_css('box-shadow', $(this).val()+'px '+$(this).val()+'px '+$(this).val()+'px #333');
        
    });
    $('#button-transition-property').change(function(){
        load_css('transition-property', $(this).val());
    });
    $('#button-transition-duration').change(function(){
        load_css('transition-duration', $(this).val()+'s');
    });
    $('#button-transition-timing').change(function(){
        load_css('transition-timing-function', $(this).val());
    });
    $('#button-transition-delay').change(function(){
        load_css('transition-delay', $(this).val()+'s');
    });
    
    $('#button-transform-rotate').change(function(){
        
        load_css('transform', 'rotate('+$(this).val()+'deg)');
        load_css('-ms-transform', 'rotate('+$(this).val()+'deg)');
        load_css('-webkit-transform', 'rotate('+$(this).val()+'deg)');
        
    });
    
    $('#button-transform-rotatex').change(function(){
        
       load_css('transform', 'rotateX('+$(this).val()+'deg)');
        load_css('-webkit-transform', 'rotateX('+$(this).val()+'deg)');
        
    });
    
    $('#button-transform-skew').change(function(){
        
        load_css('transform', 'skew('+$(this).val()+'deg)');
        load_css('-ms-transform', 'skew('+$(this).val()+'deg)');
        load_css('-webkit-transform', 'skew('+$(this).val()+'deg)');
        
    });
    
    $('#button-transform-translatex').change(function(){
        
        translate_x = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'translate('+translate_x+'px, '+translate_y+'px)');
        load_css('-ms-transform', 'translate('+translate_x+'px, '+translate_y+'px)');
        load_css('-webkit-transform', 'translate('+translate_x+'px, '+translate_y+'px)');
        
    });
    
    $('#button-transform-translatey').change(function(){
        
        translate_y = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'translate('+translate_x+'px, '+translate_y+'px)');
        load_css('-ms-transform', 'translate('+translate_x+'px, '+translate_y+'px)');
        load_css('-webkit-transform', 'translate('+translate_x+'px, '+translate_y+'px)');
        
    });
    
    $('#button-transform-scalex').change(function(){
        
        scale_x = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'scale('+scale_x+', '+scale_y+')');
        load_css('-ms-transform', 'scale('+scale_x+', '+scale_y+')');
        load_css('-webkit-transform', 'scale('+scale_x+', '+scale_y+')');
        
    });
    
    $('#button-transform-scaley').change(function(){
        
        scale_y = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'scale('+scale_x+', '+scale_y+')');
        load_css('-ms-transform', 'scale('+scale_x+', '+scale_y+')');
        load_css('-webkit-transform', 'scale('+scale_x+', '+scale_y+')');
        
    });
    
    $('#button-transform-originx').change(function(){
        
        origin_x = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform-origin', origin_x+'% '+origin_y+'% '+origin_z);
        load_css('-ms-transform-origin', origin_x+'% '+origin_y+'% '+origin_z);
        load_css('-webkit-transform-origin', origin_x+'% '+origin_y+'% '+origin_z+'px');
        
    });
    
    $('#button-transform-originy').change(function(){
        
        origin_y = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform-origin', origin_x+'% '+origin_y+'% '+origin_z);
        load_css('-ms-transform-origin', origin_x+'% '+origin_y+'% '+origin_z);
        load_css('-webkit-transform-origin', origin_x+'% '+origin_y+'% '+origin_z+'px');
        
    });
    
    $('#button-transform-originz').change(function(){
        
        origin_z = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform-origin', origin_x+'% '+origin_y+'% '+origin_z);
        load_css('-ms-transform-origin', origin_x+'% '+origin_y+'% '+origin_z);
        load_css('-webkit-transform-origin', origin_x+'% '+origin_y+'% '+origin_z+'px');
        
    });
    
    $('#button-transform-style').change(function(){
        
        load_css('transform-style', $(this).val());
        load_css('-webkit-transform-style', $(this).val());
        
    });
    
    $('#button-perspective').change(function(){
        
        load_css('perspective', $(this).val());
        load_css('-webkit-perspective', $(this).val());
        
    });
    
    $('#button-perspective-originx').change(function(){
        
        perspective_origin_x = $(this).val();
        
        load_css('perspective-origin', perspective_origin_x+'% '+perspective_origin_y+'%');
        load_css('-webkit-perspective-origin', perspective_origin_x+'% '+perspective_origin_y+'%');
        
    });
    $('#button-perspective-originy').change(function(){
        
        perspective_origin_y = $(this).val();
        
        load_css('perspective-origin', perspective_origin_x+'% '+perspective_origin_y+'%');
        load_css('-webkit-perspective-origin', perspective_origin_x+'% '+perspective_origin_y+'%');
        
    });
    
    $('#button-backface').change(function(){
        
        load_css('backface-visibility', $(this).val(), true);
        load_css('-moz-backface-visibility', $(this).val(), true);
        load_css('-ms-backface-visibility', $(this).val(), true);
        load_css('-webkit-backface-visibility', $(this).val(), true);
        
    });
    
    
   
    
    // ----------------------------------------------------- HOVER STATES ----------------------------------------------------- //
    
    $('#hover-button-fontsize').keyup(function(){
       
       load_css('font-size', $(this).val()+'px', true);
        
    });
    $('#hover-button-fontsize').change(function(){
       
       load_css('font-size', $(this).val()+'px', true);
        
    });
    
    $('#hover-button-color').change(function(){
       
       load_css('color', $(this).val(), true);
        
    });
    
    $('#hover-button-bgcolor').change(function(){
       
       load_css('background-color', $(this).val(), true);
        
    });
    
    $('#hover-button-width').change(function(){
       
       load_css('width', $(this).val(), true);
        
    });
    
    $('#hover-button-height').change(function(){
        
        
        load_css('height', $(this).val(), true);
        load_css('line-height', $(this).val()+'px', true);
        
    });
    
    $('#hover-button-border').change(function(){
        
        load_css('border-width', $(this).val()+'px', true);
        
    });
    $('#hover-button-bordercolor').change(function(){
        
        load_css('border-color', $(this).val(), true);
        
    });
    $('#hover-button-bordertype').change(function(){
        
        load_css('border-style', $(this).val(), true);
        
    });
    $('#hover-button-borderradius').change(function(){
        
        load_css('border-radius', $(this).val()+'px', true);
        
    });
    
    $('#hover-button-opacity').change(function(){
        
        load_css('opacity', $(this).val(), true);
        
    });
    $('#hover-button-boxshadow').change(function(){
        
        load_css('-webkit-box-shadow', $(this).val()+'px '+$(this).val()+'px '+$(this).val()+'px #333', true);
        load_css('-moz-box-shadow', $(this).val()+'px '+$(this).val()+'px '+$(this).val()+'px #333', true);
        load_css('box-shadow', $(this).val()+'px '+$(this).val()+'px '+$(this).val()+'px #333', true);
        
    });
    
    $('#hover-button-transform-rotate').change(function(){
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'rotate('+$(this).val()+'deg)', true);
        load_css('-ms-transform', 'rotate('+$(this).val()+'deg)', true);
        load_css('-webkit-transform', 'rotate('+$(this).val()+'deg)', true);
        
    });
    
    $('#hover-button-transform-rotatex').change(function(){
        
       load_css('transform', 'rotateX('+$(this).val()+'deg)', true);
       load_css('-webkit-transform', 'rotateX('+$(this).val()+'deg)', true);
        
    });
    
    $('#hover-button-transform-skew').change(function(){
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'skew('+$(this).val()+'deg)', true);
        load_css('-ms-transform', 'skew('+$(this).val()+'deg)', true);
        load_css('-webkit-transform', 'skew('+$(this).val()+'deg)', true);
        
    });
    
    $('#hover-button-transform-translatex').change(function(){
        
        translate_x = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'translate('+translate_x+'px, '+translate_y+'px)', true);
        load_css('-ms-transform', 'translate('+translate_x+'px, '+translate_y+'px)', true);
        load_css('-webkit-transform', 'translate('+translate_x+'px, '+translate_y+'px)', true);
        
    });
    
    $('#hover-button-transform-translatey').change(function(){
        
        translate_y = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'translate('+translate_x+'px, '+translate_y+'px)', true);
        load_css('-ms-transform', 'translate('+translate_x+'px, '+translate_y+'px)', true);
        load_css('-webkit-transform', 'translate('+translate_x+'px, '+translate_y+'px)', true);
        
    });
    
    $('#hover-button-transform-scalex').change(function(){
        
        scale_x = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'scale('+scale_x+', '+scale_y+')', true);
        load_css('-ms-transform', 'scale('+scale_x+', '+scale_y+')', true);
        load_css('-webkit-transform', 'scale('+scale_x+', '+scale_y+')', true);
        
    });
    
    $('#hover-button-transform-scaley').change(function(){
        
        scale_y = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform', 'scale('+scale_x+', '+scale_y+')', true);
        load_css('-ms-transform', 'scale('+scale_x+', '+scale_y+')', true);
        load_css('-webkit-transform', 'scale('+scale_x+', '+scale_y+')', true);
        
    });
    
    $('#hover-button-transform-originx').change(function(){
        
        origin_x = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform-origin', origin_x+'% '+origin_y+'% '+origin_z, true);
        load_css('-ms-transform-origin', origin_x+'% '+origin_y+'% '+origin_z, true);
        load_css('-webkit-transform-origin', origin_x+'% '+origin_y+'% '+origin_z+'px', true);
        
    });
    
    $('#hover-button-transform-originy').change(function(){
        
        origin_y = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform-origin', origin_x+'% '+origin_y+'% '+origin_z, true);
        load_css('-ms-transform-origin', origin_x+'% '+origin_y+'% '+origin_z, true);
        load_css('-webkit-transform-origin', origin_x+'% '+origin_y+'% '+origin_z+'px', true);
        
    });
    
    $('#hover-button-transform-originz').change(function(){
        
        origin_z = $(this).val();
        
        //var t = $('#button').css('transform');
        //if(t==='none'){ t = ''; }
        
        load_css('transform-origin', origin_x+'% '+origin_y+'% '+origin_z, true);
        load_css('-ms-transform-origin', origin_x+'% '+origin_y+'% '+origin_z, true);
        load_css('-webkit-transform-origin', origin_x+'% '+origin_y+'% '+origin_z+'px', true);
        
    });
    
    $('#hover-button-transform-style').change(function(){
        
        load_css('transform-style', $(this).val(), true);
        load_css('-webkit-transform-style', $(this).val(), true);
        
    });
    
    $('#hover-button-perspective').change(function(){
        
        load_css('perspective', $(this).val(), true);
        load_css('-webkit-perspective', $(this).val(), true);
        
    });
    
    $('#hover-button-perspective-originx').change(function(){
        
        perspective_origin_x = $(this).val();
        
        load_css('perspective-origin', perspective_origin_x+'% '+perspective_origin_y+'%', true);
        load_css('-webkit-perspective-origin', perspective_origin_x+'% '+perspective_origin_y+'%', true);
        
    });
    $('#hover-button-perspective-originy').change(function(){
        
        perspective_origin_y = $(this).val();
        
        load_css('perspective-origin', perspective_origin_x+'% '+perspective_origin_y+'%', true);
        load_css('-webkit-perspective-origin', perspective_origin_x+'% '+perspective_origin_y+'%', true);
        
    });
    
    $('#hover-button-backface').change(function(){
        
        load_css('backface-visibility', $(this).val(), true);
        load_css('-moz-backface-visibility', $(this).val(), true);
        load_css('-ms-backface-visibility', $(this).val(), true);
        load_css('-webkit-backface-visibility', $(this).val(), true);
        
    });
    
    
    
    
    // USEFUL FUNCTIONS!
    
    $('#button').mouseenter(function(){
       for(var index in button_hover_css){
            $('#button').css(index, button_hover_css[index]);
        }
    });
    $('#button').mouseleave(function(){
       for(var index in button_css){
            $('#button').css(index, button_css[index]);
       }
    });
    
    $('.hover-states').click(function(){
        if(states === 'default'){
            $('#default-states-form').slideUp(1000);
            $('#hover-states-form').slideDown(1000);
            states = 'hover';
        } else {
            $('#default-states-form').slideDown(1000);
            $('#hover-states-form').slideUp(1000);
            states = 'default';
        }
    });
    /*
    $('.hover-frame').click(function(){
        if(frame === 'default'){
            $('#result').slideUp(1000);
            $('#hover-result').slideDown(1000);
            frame = 'hover';
        } else {
            $('#result').slideDown(1000);
            $('#hover-result').slideUp(1000);
            frame = 'default';
        }
    });
    */
    
    function load_css(prop, attr, hover){
        
        if(!hover){
            $('#button').css(prop, attr);
            button_css[prop] = attr;
        } else {
            //$('#hover-button').css(prop, attr);
            button_hover_css[prop] = attr;
        }
    }
    
    /* FRAME FUNCTIONS */
            
    $('#frame-bgcolor').change(function(){
        $('#result').css('background-color', $(this).val());
    });
    
    
});


